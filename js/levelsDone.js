define([], function() {
    var STORAGE_NAME = 'penicillin2:levelsDone:';
    return {
        mode: 'normal',
        get: function() {
            return localStorage.getItem(STORAGE_NAME + this.mode) || 0;
        },
        put: function(n) {
            var alreadyDone = this.get();
            localStorage.setItem(STORAGE_NAME + this.mode, Math.max(alreadyDone, n))
        }
    };
});