define([
    'easel',
    'underscore',
    'js/game'
], function (easel, _, game) {
    return {
        callbackStorage: {},

        initContainer: function (x, y) {
            var container = new easel.Container();
            container.x = x;
            container.y = y;
            return container
        },

        initCircle: function (x, y, size, color) {
            var circle = new easel.Shape();
            circle.graphics.beginFill(color).drawCircle(x, y, size);
            return circle;
        },

        initText: function (x, y, value) {
            var text = new easel.Text(value, "25px Arial", "#ffffff");
            text.x = x;
            text.y = y;
            text.textAlign = 'center';
            return text;
        },

        drawLine: function (point1, point2, size, color) {
            var line = new easel.Shape();
            line.graphics.setStrokeStyle(size || 1);
            line.graphics.beginStroke(color || '#999');
            line.graphics.moveTo(point1.x, point1.y);
            line.graphics.lineTo(point2.x, point2.y);
            line.graphics.endStroke();
            return line;
        },

        getDistance: function(point1, point2) {
            return Math.sqrt(Math.pow(point1.x - point2.x, 2) + Math.pow(point1.y - point2.y, 2));
        },

        setInterval: function (callback, time, context) {
            var currentTime = time;
            var id = _.uniqueId();
            var fn = function (delta) {
                currentTime -= delta;
                if (currentTime <= 0) {
                    callback.call(context || {});
                    currentTime = time;
                }
            };
            game.on('animation', fn);
            this.callbackStorage[id] = fn;
            return id;
        },

        clearInterval: function (id) {
            var fn = this.callbackStorage[id];
            game.off('animation', fn);
            delete this.callbackStorage[id];
        },

        /**
         * Backbone extend
         * Helper function to correctly set up the prototype chain, for subclasses.
         * @param protoProps
         * @param staticProps
         * @returns {*}
         */
        extend: function (protoProps, staticProps) {
            var parent = this;
            var child;

            // The constructor function for the new subclass is either defined by you
            // (the "constructor" property in your `extend` definition), or defaulted
            // by us to simply call the parent's constructor.
            if (protoProps && _.has(protoProps, 'constructor')) {
                child = protoProps.constructor;
            } else {
                child = function () {
                    return parent.apply(this, arguments);
                };
            }

            // Add static properties to the constructor function, if supplied.
            _.extend(child, parent, staticProps);

            // Set the prototype chain to inherit from `parent`, without calling
            // `parent`'s constructor function.
            var Surrogate = function () {
                this.constructor = child;
            };
            Surrogate.prototype = parent.prototype;
            child.prototype = new Surrogate;

            // Add prototype properties (instance properties) to the subclass,
            // if supplied.
            if (protoProps) _.extend(child.prototype, protoProps);

            // Set a convenience property in case the parent's prototype is needed
            // later.
            child.__super__ = parent.prototype;

            return child;

        }
    }
});