define([
        'js/models/sides'
    ],
    function (sides) {
            return {
                name: 'Do it faster!',
                number: 7,
                colonies: [
                    {x: 160, y:150, side: sides.purple, population: 20},
                    {x: 160, y:300, population: 5},
                    {x: 160, y:450, population: 3},

                    {x: 320, y:300, population: 5},
                    {x: 320, y:450, population: 3},

                    {x: 480, y:300, population: 5},
                    {x: 480, y:450, population: 3},

                    {x: 640, y:150, side: sides.green, population: 20},
                    {x: 640, y:300, population: 5},
                    {x: 640, y:450, population: 3},
                ],
                bots: [
                    {
                        side: sides.green,
                        difficult: 2
                    }
                ]
            };
        
    });