define([
        'js/models/sides',
        'js/game',
        'js/interface/interface',
        'js/levelsDone',
        'text!js/levels/1/firstTip.html',
        'text!js/levels/1/secondTip.html'
    ],
    function (sides, game, interfaceView, levelsDone, firstTip, secondTip) {
        return {
            name: 'First challenge!',
            number: 1,
            colonies: [
                {x: 100, y: 300, side: sides.purple, population: 25},
                {x: 300, y: 300},
                {x: 300, y: 150},
                {x: 300, y: 450},
                {x: 500, y: 300},
                {x: 700, y: 300, side: sides.green}
            ],
            bots: [
                {
                    side: sides.green,
                    difficult: 1
                }
            ],

            initialize: function () {
                if (levelsDone.get() === 0) {
                    gameHowto();
                }
            }
        };

        function gameHowto() {
            game.pause(true);
            game.stage.update();
            var message = interfaceView.showMessage(firstTip, function () {
                message.innerHTML = secondTip;
                message.onclick = startGame;
            });
            var startGame = function () {
                game.pause(false);
                document.getElementById('field').removeChild(message);
            }
        }
    });