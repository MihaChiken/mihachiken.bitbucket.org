define([
        'js/models/sides'
    ],
    function (sides) {
            return {
                name: 'Classic duel',
                number: 8,
                colonies: [
                    {x: 100, y: 300, side: sides.purple},
                    {x: 300, y: 200},
                    {x: 300, y: 300},
                    {x: 300, y: 400},
                    {x: 500, y: 200},
                    {x: 500, y: 300},
                    {x: 500, y: 400},
                    {x: 700, y: 300, side: sides.green}
                ],
                bots: [
                    {
                        side: sides.green,
                        difficult: 2
                    }
                ]
            };
    });