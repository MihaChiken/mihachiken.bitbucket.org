define([
        'js/models/sides'
    ],
    function (sides) {
        return {
            name: '',
            number: 3,
            colonies: [
                {x: 160, y: 150, side: sides.purple},
                {x: 160, y: 450, side: sides.purple},
                {x: 320, y: 450},
                {x: 320, y: 150},
                {x: 400, y: 300, population: 5},
                {x: 480, y: 450},
                {x: 480, y: 150},
                {x: 640, y: 150, side: sides.green},
                {x: 640, y: 450, side: sides.green}
            ],
            bots: [
                {
                    side: sides.green,
                    difficult: 1
                }
            ]
        };
    });