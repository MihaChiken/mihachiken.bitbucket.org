define([
        'js/models/sides'
    ],
    function (sides) {
            return {
                name: 'Double trouble!',
                number: 6,
                colonies: [
                    {x: 150, y: 300},
                    {x: 275, y: 300, side: sides.purple},
                    {x: 275, y: 175},
                    {x: 275, y: 425},
                    {x: 400, y: 300},
                    {x: 550, y: 175, side: sides.green},
                    {x: 550, y: 425, side: sides.brown},
                    {x: 675, y: 120},
                    {x: 675, y: 240},
                    {x: 675, y: 360},
                    {x: 675, y: 480}
                ],
                bots: [
                    {
                        side: sides.green,
                        difficult: 1
                    },
                    {
                        side: sides.brown,
                        difficult: 1
                    }
                ]
            };
    });