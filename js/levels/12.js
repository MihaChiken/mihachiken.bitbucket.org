define([
        'js/models/sides',
        'js/game',
        'js/interface/interface',
        'easel',
        'js/controllers/SpawnController',
        'js/models/ColonyModel'
    ],
    function (sides, game, interfaceView, easel, Spawn, ColonyModel) {
            var level = {
                name: 'Survive at any cost',
                number: 9,
                colonies: [
                   {x: 165, y: 65, side: sides.green, population: 1},
                   {x: 65, y: 165, side: sides.green, population: 1},

                   {x: 635, y: 535, side: sides.green, population: 1},
                   {x: 735, y: 435, side: sides.green, population: 1},

                   {x: 635, y: 65, side: sides.green, population: 1},
                   {x: 735, y: 165, side: sides.green, population: 1},

                   {x: 165, y: 535, side: sides.green, population: 1},
                   {x: 65, y: 435, side: sides.green, population: 1},

                   {x: 300, y: 240, population: 5},
                   {x: 300, y: 360, population: 5},
                   {x: 400, y: 180, population: 5},
                   {x: 400, y: 300, side: sides.purple, population: 150},
                   {x: 400, y: 420, population: 5},
                   {x: 500, y: 240, population: 5},
                   {x: 500, y: 360, population: 5}

                ],
                bots: [
                    {
                        side: sides.green,
                        difficult: 2
                    }
                ],

                initialize: function(colonies, spawns) {
                    this.coloniesControllers = colonies;
                    this.spawnsControllers = spawns;

                    game.pause(true);
                    game.stage.update();
                    var message = interfaceView.showMessage('Survive for 60 seconds', function () {
                        game.pause(false);
                        document.getElementById('field').removeChild(message);

                        initCoutDown();
                    });
                }
            };
        return level;

        function initCoutDown() {
            var text = new easel.Text(60, "40px Arial", "#ffffff");
            text.x = 400;
            text.y = 50;
            text.textAlign = 'center';
            game.stageAdd(text);
            var mileSecondsLast = 60000;
            var countDownUpdate = function(delta) {
                mileSecondsLast -= delta;
                text.text = Math.floor(mileSecondsLast / 1000);
                if (text.text <= 0) {
                    game.off('animation', countDownUpdate);
                    callReinforcement();
                    game.stageRemove(text);
                }
            };
            game.on('animation', countDownUpdate);
            game.on('destroy', function() {
                game.off('animation', countDownUpdate);
            });
        }

        function callReinforcement() {
            game.pause(true);
            var message = interfaceView.showMessage('Good job!! <br> And now, finish HIM!', function () {
                game.pause(false);
                document.getElementById('field').removeChild(message);

                createNewSpawns();
            });
        }

        function createNewSpawns() {
            var spawnsAmount = 7;
            var fakeColony = new ColonyModel({
                x: 0,
                y:300,
                population: 100 * spawnsAmount,
                side: sides.getPlayerSide()
            });

            for(var i = 0; i < spawnsAmount; i++) {
                var divider = spawnsAmount - i;
                var target = level.coloniesControllers[i].model;
                setTimeout(function(target, divider) {
                    level.spawnsControllers.push(new Spawn({
                        'fromColony': fakeColony,
                        'toColony': target,
                        'populationDivider': divider || 1
                    }));
                }.bind(this, target, divider), (300 * i) + 1);
            }
        }
    });