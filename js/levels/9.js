define([
        'js/models/sides'
    ],
    function (sides) {
            return {
                name: 'Hexagon',
                number: 9,
                colonies: [
                   {x: 160, y: 300, side: sides.purple},
                   {x: 310, y: 240},
                   {x: 310, y: 360},
                   {x: 410, y: 180},
                   {x: 410, y: 300},
                   {x: 410, y: 420},
                   {x: 510, y: 240},
                   {x: 510, y: 360},
                   {x: 640, y: 90, side: sides.green},
                   {x: 640, y: 530, side: sides.brown}
                ],
                bots: [
                    {
                        side: sides.green,
                        difficult: 2
                    },
                    {
                        side: sides.brown,
                        difficult: 2
                    }
                ]
            };
    });