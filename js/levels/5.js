define([
        'js/models/sides'
    ],
    function (sides) {
        return {
            name: 'Bridge',
            number: 5,
            colonies: [
                {x: 133, y: 150, side: sides.purple},
                {x: 133, y: 300},
                {x: 133, y: 450, side: sides.purple},
                {x: 266, y: 300},
                {x: 400, y: 300},
                {x: 533, y: 300},
                {x: 666, y: 150, side: sides.brown},
                {x: 666, y: 300},
                {x: 666, y: 450, side: sides.brown}
            ],
            bots: [
                {
                    side: sides.brown,
                    difficult: 2
                }
            ]
        };
    });