define([
        'js/models/sides',
        'js/game',
        'js/interface/interface',
        'js/levelsDone',
        'text!js/levels/2/tip.html'
    ],
    function (sides, game, interfaceView, levelsDone, tip) {
        return {
            name: 'Two ways',
            number: 2,
            colonies: [
                {x: 133, y: 300, side: sides.purple, population: 30},
                {x: 266, y: 175},
                {x: 400, y: 175},
                {x: 533, y: 175},
                {x: 266, y: 425},
                {x: 400, y: 425},
                {x: 533, y: 425},
                {x: 666, y: 300, side: sides.green}
            ],
            bots: [
                {
                    side: sides.green,
                    difficult: 1
                }
            ],

            initialize: function () {
                if (levelsDone.get() < 2) {
                    gameHowto();
                }
            }
        };

        function gameHowto() {
            game.pause(true);
            game.stage.update();
            var message = interfaceView.showMessage(tip, function () {
                game.pause(false);
                document.getElementById('field').removeChild(message);
            });
        }
    });