define([
        'js/models/sides'
    ],
    function (sides) {
        return {
            name: 'Disco',
            number: 11,
            colonies: [
                {x: 200, y: 240, side: sides.purple},
                {x: 200, y: 360, side: sides.purple},

                {x: 320, y: 120, side: sides.pink},
                {x: 320, y: 240},
                {x: 320, y: 360},
                {x: 320, y: 480, side: sides.brown},

                {x: 440, y: 120, side: sides.pink},
                {x: 440, y: 240},
                {x: 440, y: 360},
                {x: 440, y: 480, side: sides.brown},

                {x: 560, y: 240, side: sides.green},
                {x: 560, y: 360, side: sides.green}

            ],
            bots: [
                {
                    side: sides.pink,
                    difficult: 2
                },
                {
                    side: sides.brown,
                    difficult: 2
                },
                {
                    side: sides.green,
                    difficult: 2
                }
            ]
        };
    });