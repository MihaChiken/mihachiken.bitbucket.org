define([
        'js/models/sides'
    ],
    function (sides) {
        return {
            name: 'Two diamonds',
            number: 4,
            colonies: [
                {x: 300, y: 300, side: sides.purple},
                {x: 100, y: 300, population: 4},
                {x: 200, y: 150, population: 5},
                {x: 200, y: 450, population: 5},
                {x: 500, y: 300, side: sides.green},
                {x: 700, y: 300, population: 4},
                {x: 600, y: 150, population: 5},
                {x: 600, y: 450, population: 5},
            ],
            bots: [
                {
                    side: sides.green,
                    difficult: 1
                }
            ]
        };
    });