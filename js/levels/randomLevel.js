define([
    'underscore',
    'js/models/sides',
    'js/utils',
    'js/levelsDone'
], function(_, sides, utils, levelsDone) {
    var BOT_DIFFICULT = {
        normal: 2,
        hard: 3
    };
    var COLONIES_MINIMUM_AMOUNT = 8;
    var COLONIES_MAXIMUM_AMOUNT = 15;
    var FIELD_MARGIN = 50;
    var MIN_DISTANCE_BETWEEN_COLONY = 130;

    return function() {
        var botsAmount = _.sample([1, 1, 1, 1, 2, 2, 3]);
        var availableSides = [sides.green, sides.brown, sides.pink].slice(0, botsAmount);

        return {
            name: '',
            number: -1,
            colonies: initializeColonies(availableSides),
            bots: initializeBots(availableSides)
        };
    };


    function initializeBots(availableSides) {
        return _.map(availableSides, function(side) {
            return {
                side: side,
                difficult: BOT_DIFFICULT[levelsDone.mode]
            }
        });
    }

    function initializeColonies(availableSides) {
        var coloniesPerSide = _.random(1, 5 - availableSides.length);
        var coloniesMinimumAmount = Math.max((availableSides.length + 1) * coloniesPerSide, COLONIES_MINIMUM_AMOUNT);
        var coloniesAmount = _.random(coloniesMinimumAmount, COLONIES_MAXIMUM_AMOUNT);
        return createColonies(coloniesAmount, coloniesPerSide, availableSides);
    }

    function createColonies(coloniesAmount, coloniesPerSide, availableSides) {
        var colonyBySides = getColonyBySides(coloniesAmount, coloniesPerSide, availableSides);
        var colonies = [];
        _.each(colonyBySides, function(side) {
            while(true) {
                var colony = {
                    x: _.random(FIELD_MARGIN, 800 - FIELD_MARGIN),
                    y: _.random(FIELD_MARGIN, 600 - FIELD_MARGIN),
                    side: side
                };
                if (validateColony(colony, colonies)) {
                    colonies.push(colony);
                    return;
                }
            }
        });
        return colonies
    }

    function getColonyBySides(coloniesAmount, coloniesPerSide, availableSides) {
        var activeSides = _.union([sides.getPlayerSide()], availableSides);
        var coloniesSides = [];
        _.times(coloniesPerSide, function() {
            coloniesSides = coloniesSides.concat(activeSides);
        });

        console.log(availableSides, coloniesSides, coloniesPerSide);
        _.times(coloniesAmount - coloniesSides.length, function() {
            coloniesSides.push(sides.default);
        });

        return coloniesSides;
    }

    function validateColony(colony, colonies) {
        return _.every(colonies, function(otherColony) {
            return utils.getDistance(colony, otherColony) >= MIN_DISTANCE_BETWEEN_COLONY;
        });
    }

});