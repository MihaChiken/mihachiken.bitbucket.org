define([
        'js/models/sides'
    ],
    function (sides) {
            return {
                name: '',
                number: 10,
                colonies: [
                   {x: 160, y: 150},
                   {x: 160, y: 300, side: sides.purple},
                   {x: 160, y: 450},
                   {x: 320, y: 150, side: sides.purple},
                   {x: 320, y: 300},
                   {x: 320, y: 450},
                   {x: 480, y: 150},
                   {x: 480, y: 300},
                   {x: 480, y: 450, side: sides.pink},
                   {x: 640, y: 150},
                   {x: 640, y: 300, side: sides.pink},
                   {x: 640, y: 450}
                ],
                bots: [
                    {
                        side: sides.pink,
                        difficult: 2
                    }
                ]
            };
    });