define([
    'underscore',
    'js/models/sides',
    'js/controllers/SpawnController',
    'js/utils'
], function (_, sides, Spawn, utils) {
    var INTERVAL = {
        1: 2000,
        2: 1500,
        3: 800,
        4: 700
    };
    // Average coefficient that shows how many points loose spawn to get the distance unit
    // TODO make it cleverer
    var DISTANCE_K = 0.015;

    return function (side, controllers, difficult) {
        this.intervalId = utils.setInterval(function () {
            var k = Math.random();
            if (k < 0.75) {
                this.makeMove(side, controllers, difficult);
            }
        }.bind(this), INTERVAL[difficult]);

        this.destroy = function () {
            utils.clearInterval(this.intervalId);
        };

        this.makeMove = function (mySide, controllers, difficult) {
            var colonies = _.pluck(controllers, 'model');
            var myColonies = _.filter(colonies, function (colony) {
                return colony.get('side') === mySide;
            });
            var enemyColonies = _.difference(colonies, myColonies);
            var possibleAttacks = _.sortBy(this.getPossibleAttacks(myColonies, enemyColonies),
                function (attack) {
                    return -1 * attack.profit;
                });
            var doubleStrikePossibility = difficult * 0.25;
            if (possibleAttacks[0] && possibleAttacks[0].profit > 1) {
                var attacksAmount = difficult > 2 ? 2 : 1;
                this.attack(possibleAttacks, attacksAmount, difficult);
            } else if (possibleAttacks.length >= 2 && Math.random() < doubleStrikePossibility) {
                this.doubleStrike(possibleAttacks);
            }
        };

        this.attack = function(possibleAttacks, attacksAmount, difficult) {
            var attackedColonies = [], attackingColonies = [];
            for (var i = 0;
                 i < possibleAttacks.length && attackingColonies.length < attacksAmount;
                 i++) {
                var attack = this.chooseAttackVariant(possibleAttacks, difficult);
                if (attack &&
                    !_.contains(attackingColonies, attack.myColony) &&
                    !_.contains(attackedColonies, attack.enemyColony)) {
                    attackingColonies.push(attack.myColony);
                    attackedColonies.push(attack.enemyColony);
                    new Spawn({
                        fromColony: attack.myColony,
                        toColony: attack.enemyColony,
                        divider: 1
                    });
                }
            }
        };

        this.chooseAttackVariant = function(possibleAttacks, difficult) {
            var wrongAttackPossibility = 0.5 - difficult * 0.2;
            if (Math.random() > wrongAttackPossibility) {
                if (possibleAttacks[0].profit > 1) {
                    return possibleAttacks[0];
                }
                return false;
            }
            return possibleAttacks[possibleAttacks.length / 2];
        };

        this.doubleStrike = function(possibleAttacks) {
            for (var i = 0; i < possibleAttacks.length; i++) {
                var firstAttack = possibleAttacks[i];
                var secondAttack = _.find(possibleAttacks, function(attack) {
                    return attack.enemyColony === firstAttack.enemyColony &&
                        attack.myColony !== firstAttack.myColony;
                });
                if (secondAttack &&
                    Math.abs(firstAttack.profit + secondAttack.profit) < firstAttack.enemyColony.get('population')) {
                    new Spawn({
                        fromColony: firstAttack.myColony,
                        toColony: secondAttack.enemyColony
                    });
                    new Spawn({
                        fromColony: secondAttack.myColony,
                        toColony: secondAttack.enemyColony
                    });
                    return;
                }
            }
        };

        this.getPossibleAttacks = function (myColonies, enemyColonies) {
            var possibleAttacks = [];
            _.each(myColonies, function (myColony) {
                _.each(enemyColonies, function (enemyColony) {
                    var distance = utils.getDistance(myColony.getPoint(), enemyColony.getPoint());
                    // While spawn will be flying to enemy colony the points in there will increase
                    // (if it isn`t a default colony)
                    if (enemyColony.side !== sides.default) {
                        distance *= 2;
                    }
                    possibleAttacks.push({
                        profit: myColony.get('population') - enemyColony.get('population') - distance * DISTANCE_K,
                        myColony: myColony,
                        enemyColony: enemyColony
                    });
                })
            }, this);
            return possibleAttacks;
        }
    };

});