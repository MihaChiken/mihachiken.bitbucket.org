define([
    'underscore',
    'js/events'
], function(_, events) {
    var keyBindings = {
        keyMap: {
            112: 'pause',
            1079: 'pause',
            16: 'shift'
        },

        pressed: {
            'shift': false
        },

        initHotKey: function(codes, name) {
            _.each(codes, function(code) {
                this.keyMap[code] = name;
            }, this)
        }

    };
    _.extend(keyBindings, events);

    document.onkeypress = function(e) {
        if(keyBindings.keyMap[e.keyCode]) {
            keyBindings.trigger(keyBindings.keyMap[e.keyCode]);
        }
    };

    document.onkeydown = function(e) {
        var eventName = keyBindings.keyMap[e.keyCode];
        if(eventName) {
            keyBindings.trigger(eventName + ':keydown');
            if(keyBindings.pressed[eventName] === false) {
                keyBindings.pressed[eventName] = true;
            }
        }
    };

    document.onkeyup = function(e) {
        var eventName = keyBindings.keyMap[e.keyCode];
        if(eventName) {
            keyBindings.trigger(eventName + ':keyup');
            if(keyBindings.pressed[eventName]) {
                keyBindings.pressed[eventName] = false;
            }
        }
    };
    return keyBindings;
});