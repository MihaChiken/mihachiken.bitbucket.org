define([
    'js/controllers/Controller',
    'js/views/ColonyView',
    'js/models/ColonyModel',
    'js/utils'
], function(Controller, View, Model, utils) {
    return Controller.extend({
        Model: Model,

        initialize: function(model) {
            Controller.prototype.initialize.call(this, model);
            this.renderView(View);
            this.setGrowthInterval();
            this.model.on('change:side', this.onChangeSide, this);
            this.model.on('change:growthTime', this.setGrowthInterval, this);
        },

        renderView: function(View) {
            Controller.prototype.renderView.call(this, View);

            this.view.on('mousedown', this.onColonyDrag, this);
            this.view.on('mouseup', this.onColonyDrop, this);
        },

        onColonyDrag: function(model) {
            this.trigger('drag', model);
        },

        onColonyDrop: function(point) {
            this.trigger('drop', point);
        },

        setGrowthInterval: function() {
            if (this.growthIntervalId) {
                utils.clearInterval(this.growthIntervalId);
            }
            if (this.model.get('growthTime') > 0) {
                this.growthIntervalId = utils.setInterval(this.model.populationGrowth, this.model.get('growthTime'),
                    this.model);
            }
        },

        onChangeSide: function() {
            this.view.off('mousedown', this.onColonyDrag);
            this.view.off('mouseup', this.onColonyDrop);
            this.view.destroy();

            this.renderView(View);
            this.trigger('colonySideChanged', this.model.get('side'));
        },

        destroy: function() {
            this.model.off('change:side', this.onChangeSide, this);
            utils.clearInterval(this.growthIntervalId);
            Controller.prototype.destroy.call(this);
        },

        isPointInColony: function(point) {
            return this.view.checkPoint(point);
        }
    });
});