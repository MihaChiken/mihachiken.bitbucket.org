define([
    'js/controllers/Controller',
    'js/views/SpawnView',
    'js/models/SpawnModel',
    'js/utils'
], function (Controller, View, Model, utils) {
    var COLONY_MAX_POPULATION = 150;
    return Controller.extend({

        Model: Model,

        initialize: function (model) {
            Controller.prototype.initialize.call(this, model);
            var fromColony = this.model.get('fromColony');
            fromColony.set('population', fromColony.get('population') - this.model.get('population'));

            this.renderView(View);
            this.setDecreaseInterval();
            this.view.on('attackColony', this.attackColony, this);
        },

        setDecreaseInterval: function () {
            if (this.model.get('decreaseTime') > 0) {
                this.decreaseIntervalId = utils.setInterval(this.populationUpdate.bind(this), this.model.get('decreaseTime'),
                    this.model);
            }
        },

        populationUpdate: function () {
            if (!this.model.populationUpdate()) {
                this.destroy();
            }
        },

        attackColony: function () {
            var spawnPopulation = this.model.get('population');
            var toColony = this.model.get('toColony');

            if (spawnPopulation > 0) {
                if (this.model.get('side') === toColony.get('side')) {
                    toColony.set('population', Math.min(toColony.get('population') + spawnPopulation, COLONY_MAX_POPULATION));
                } else if (spawnPopulation <= toColony.get('population')) {
                    toColony.set('population', toColony.get('population') - spawnPopulation);
                } else {
                    toColony.set({
                        side:       this.model.get('side'),
                        population: spawnPopulation - toColony.get('population')
                    });
                }
            }
            this.destroy();
        },

        destroy: function () {
            this.view.off('attackColony', this.attackColony);
            utils.clearInterval(this.decreaseIntervalId);
            Controller.prototype.destroy.call(this);
        }
    });
});