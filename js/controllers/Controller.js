define([
    'underscore',
    'js/utils',
    'js/events'
], function(_, utils, events) {
    var Controller = function(model) {
        this.initialize(model);
    };

    _.extend(Controller.prototype, events);

    Controller.prototype.initialize = function(model) {
        if (!model.cid && this.Model) {
            model = new this.Model(model);
        }
        this.model = model;
    };

    Controller.prototype.renderView = function(View) {
        this.view = new View(this.model);
        this.view.render();
    };

    Controller.prototype.destroy = function() {
        this.view.destroy();
    };

    Controller.extend = utils.extend.bind(Controller);

    return Controller;
});