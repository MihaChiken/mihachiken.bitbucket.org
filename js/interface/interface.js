define([
    'underscore',
    'js/events',
    'js/levelsDone'
], function (_, events, levelsDone) {
    return _.extend({
        levelsAmount: 12,
        toolbarMap: {
            'pause': 'Pause',
            'backToLevels': '< Back to levels'
        },

        renderLevelList: function () {
            document.getElementById('header').className = '';
            var field = document.getElementById('field');
            field.innerHTML = '';
            field.className = '';
            var onLevelClick = function (e) {
                this.trigger('levelSelected', parseInt(e.target.innerHTML, 10));
            }.bind(this);
            for (var i = 1; i <= this.levelsAmount; i++) {
                var level = document.createElement('div');
                level.className = 'level';
                level.innerHTML = i;
                if (levelsDone.get() >= i - 1) {
                    level.className = 'level active';
                    level.onclick = onLevelClick;
                }
                field.appendChild(level);
            }
        },

        renderToolbar: function () {
            document.getElementById('header').className = 'hidden';
            var toolbar = document.getElementById('toolbar');
            _.each(this.toolbarMap, function (value, key) {
                var button = this.createButton(key, value);
                toolbar.appendChild(button);
            }, this);

            this.on('pause:clicked', this.togglePause);
        },

        togglePause: function () {
            var pauseButton = document.getElementById('pause');
            pauseButton.className = pauseButton.className === 'button' ? 'button active' : 'button';
        },

        createButton: function (id, text) {
            var button = document.createElement('div');
            button.className = 'button';
            button.id = id;
            button.innerHTML = text;
            button.onclick = this.buttonClicked.bind(this);
            return button;
        },

        buttonClicked: function (e) {
            this.trigger(e.target.id + ':clicked');
        },

        destroyToolbar: function () {
            document.getElementById('toolbar').innerHTML = '';
        },

        updateFPS: function (fps) {
//            document.getElementById('fps').innerHTML = fps;
        },

        endLevel: function(message) {
            var text = message + '<br><small>click here to choose level</small>';
            this.showMessage(text, this.renderLevelList.bind(this));
        },

        showMessage: function (message, callback) {
            var messageDiv = document.createElement('div');
            messageDiv.className = 'message';
            messageDiv.innerHTML = message;
            messageDiv.onclick = callback;
            document.getElementById('field').appendChild(messageDiv);
            return messageDiv;
        }
    }, events);

    function clickPause() {
        document.getElementById('pause').click();
    }
});