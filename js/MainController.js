define([
        'underscore'
    ], function (_) {
        return function (level) {

            require([
                'js/game',
                'js/views/DragTrackingView',
                'js/controllers/ColonyController',
                'js/controllers/SpawnController',
                'js/models/sides',
                'js/keybinding',
                'js/interface/interface',
                'js/AI/bot',
                'js/levelsDone'
            ], function (game, DragTrackingView, ColonyController, SpawnController, sides, keyBinding, interfaceView, Bot, levelsDone) {
                var coloniesControllers = [];
                var spawnControllers = [];
                var bots = [];
                var dragView = null;
                game.initialize();
                initLevel(level);

                interfaceView.renderToolbar();
                interfaceView.on('pause:clicked', game.pause, game);
                interfaceView.once('backToLevels:clicked', backToLevels);

                function initLevel(level) {
                    game.setGameName(level.name);

                    _.each(level.colonies, function (colony) {
                        var controller = new ColonyController(colony);
                        coloniesControllers.push(controller);

                        controller.on('drag', onColonyDrag);
                        controller.on('drop', onColonyDrop);
                        controller.on('colonySideChanged', onColonyChangeSide);
                    });

                    bots = _.map(level.bots, function (bot) {
                        var botDifficult = levelsDone.mode === 'hard' ? 1 : 0;
                        return new Bot(bot.side, coloniesControllers, bot.difficult + botDifficult);
                    });

                    if (_.isFunction(level.initialize)) {
                        level.initialize(coloniesControllers, spawnControllers);
                    }
                }

                function onColonyDrag(model) {
                    if (model.get('side') === sides.getPlayerSide() && !game.isPaused) {
                        dragView = new DragTrackingView(model);
                        dragView.render();
                        dragView.once('destroy', function () {
                            setTimeout(function () {
                                dragView = null;
                            }, 50);
                        });
                    }
                }

                function onColonyDrop(point) {
                    var dropColony = getColonyUnderPoint(point);
                    if (dropColony && dragView &&
                        dragView.model !== dropColony.model &&
                        dragView.model.get('side') === sides.getPlayerSide()) {
                        var divider = keyBinding.pressed.shift ? 2 : 1;
                        spawnControllers.push(new SpawnController({
                            'fromColony': dragView.model,
                            'toColony': dropColony.model,
                            'populationDivider': divider
                        }));
                    }
                }

                function getColonyUnderPoint(point) {
                    return _.find(coloniesControllers, function (colony) {
                        return colony.isPointInColony(point);
                    });
                }

                function isGameOver() {
                    var isPlayerWin = _.every(coloniesControllers, function (colonyController) {
                        var colony = colonyController.model;
                        return colony.get('side') === sides.getPlayerSide() ||
                            colony.get('side') === sides.default;
                    });
                    if (isPlayerWin) {
                        return 'win';
                    }
                    var isPlayerLoose = _.every(coloniesControllers, function (colonyController) {
                        return  colonyController.model.get('side') !== sides.getPlayerSide();
                    });
                    return isPlayerLoose ? 'lose' : false;
                }

                function onColonyChangeSide() {
                    var gameOverStatus = isGameOver();
                    if (gameOverStatus) {
                        destroyLevel(level);
                        var message = 'You lose!';
                        if (gameOverStatus === 'win') {
                            message = 'You win!';
                            levelsDone.put(level.number);
                        }
                        interfaceView.endLevel(message);
                    }
                }

                function backToLevels() {
                    destroyLevel();
                    interfaceView.renderLevelList();
                }

                function destroyLevel() {
                    interfaceView.destroyToolbar();
                    game.destroy();
                    setTimeout(function () {
                        _.invoke(coloniesControllers, 'destroy');
                        _.invoke(spawnControllers, 'destroy');
                        _.invoke(bots, 'destroy');
                        coloniesControllers = [];
                        spawnControllers = [];
                        bots = [];
                    }.bind(this), 100);
                    interfaceView.off('pause:clicked', game.pause);

                    if (_.isFunction(level.destroy)) {
                        level.destroy();
                    }

                    _.each(coloniesControllers, function (controller) {
                        controller.off('drag', onColonyDrag);
                        controller.off('drop', onColonyDrop);
                        controller.off('colonySideChanged', onColonyChangeSide);
                    });
                }

            });
        };
    }
);