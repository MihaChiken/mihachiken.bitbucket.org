define([
    'underscore',
    'easel',
    'js/events',
    'js/levelsDone'
], function (_, easel, events, levelsDone) {
    var gameView = {
        initialize: function () {
            this.canvas = document.createElement('canvas');
            this.canvas.id = 'penicillin';
            this.canvas.width = 800;
            this.canvas.height = 600;

            this.field = document.getElementById('field');
            this.field.innerHTML = '';
            this.field.className = levelsDone.mode;
            this.field.appendChild(this.canvas);
            this.stage = new easel.Stage('penicillin');
            easel.Touch.enable(this.stage);
            easel.Ticker.addEventListener("tick", this.animate.bind(this));
            easel.Ticker.setFPS(50);

            this.stage.enableMouseOver(10);
            this.timeInterval = 0;
            this.gameTime = 0;
            this.isPaused = false;
            this.alive = true;

//            window.addEventListener('resize', this.resize.bind(this), false);
        },

        stageAdd: function (element) {
            this.stage.addChild(element);
        },

        stageRemove: function (element) {
            this.stage.removeChild(element);
        },

        setGameName: function(value) {
            var text = new easel.Text(value, "28px Arial", "#ddd");
            text.x = 400;
            text.y = 20;
            text.textAlign = 'center';
            this.stageAdd(text);
        },

        /**
         * @private
         * @param event
         */
        animate: function (event) {
            if (!this.isPaused) {
                this.runAnimation(event.delta);
            }
        },

        /**
         * @private
         * @param deltaTime
         */
        runAnimation: function (deltaTime) {
            this.gameTime += deltaTime;
            this.trigger('animation', deltaTime, this.gameTime);
            this.stage.update();
        },

        pause: function (pauseValue) {
            this.isPaused = _.isBoolean(pauseValue) ? pauseValue : !this.isPaused;
        },

        destroy: function () {
            easel.Ticker.removeAllEventListeners("tick");
            this.trigger('destroy');
            this.isPaused = true;
            this.alive = false;
        }
    };

    _.extend(gameView, events);

    return gameView;
});