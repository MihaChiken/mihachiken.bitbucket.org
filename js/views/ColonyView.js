define([
    'underscore',
    'easel',
    'js/views/View',
    'js/utils'
], function (_, easel, View, utils) {
    var DEFAULT_COLONY_SIZE = 30;
    var SCALE_COEFFICIENT = 0.0045; // magic number
    return View.extend({
        render: function () {
            View.prototype.render.call(this);

            this.initCircle();
            this.text = utils.initText(0, -DEFAULT_COLONY_SIZE / 2, this.model.get('population'));

            this.container.addChild(this.circle, this.text);
            this.container.hitArea = this.circle;
            this.container.coursor = 'pointer';

            this.container.addEventListener('mousedown', function (event) {
                this.trigger('mousedown', this.model);
            }.bind(this));

            this.container.addEventListener('pressup', function (event) {
                    this.trigger('mouseup', {x: event.stageX, y:event.stageY});
            }.bind(this));
        },

        initCircle: function() {
            this.circle = utils.initCircle(0, 0, DEFAULT_COLONY_SIZE, this.model.get('side').color);

            var blurFilter = new easel.BlurFilter(5, 5, 1);
            var bounds = blurFilter.getBounds();
            this.circle.filters = [blurFilter];
            this.circle.cache(-50+bounds.x, -50+bounds.y, 100+bounds.width, 100+bounds.height);
        },

        animation: function () {
            var population = this.model.get('population');
            this.container.scaleX = 1 + population * SCALE_COEFFICIENT;
            this.container.scaleY = 1 + population * SCALE_COEFFICIENT;

            this.text.text = population;
        },

        checkPoint: function(point) {
            return utils.getDistance(point, this.container)
                <
                DEFAULT_COLONY_SIZE * (1 + this.model.get('population') * SCALE_COEFFICIENT);
        }
    });
});