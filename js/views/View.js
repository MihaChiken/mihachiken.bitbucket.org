define([
    'underscore',
    'js/events',
    'js/utils',
    'js/game' // TODO
], function(_, events, utils, game) {

    var View = function(model) {
        this.model = model;

        this.initialize();
    };

    _.extend(View.prototype, events);

    View.prototype.render = function() {
        this.trigger('render');
        this.container = utils.initContainer(this.model.get('x'), this.model.get('y'));
        game.on('animation', this.animation, this);
        game.stageAdd(this.container);
    };

    View.prototype.destroy = function() {
        this.trigger('destroy');
        game.off('animation', this.animation, this);
        game.stageRemove(this.container);
    };

    View.prototype.initialize = function() {};

    View.prototype.animation = function() {};

    View.extend = utils.extend.bind(View);

    return View;
});