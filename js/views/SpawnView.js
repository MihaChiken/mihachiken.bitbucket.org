define([
    'underscore',
    'easel',
    'js/views/View',
    'js/utils'
], function (_, easel, View, utils) {

    return View.extend({
        render: function () {
            View.prototype.render.call(this);

            this.text = utils.initText(20, 10, this.model.get('population'));
            this.container.addChild(this.text);

            this.addShadow();
            this.addSpores();
        },

        addSpores: function() {
            this.circles = [];
            for (var i = 0; i < this.model.get('population') / 2; i++) {
                var circle = utils.initCircle(_.random(-9, 9), _.random(-9, 9), _.random(2, 4),
                    this.model.get('side').color);
                this.circles.push(circle);
                this.container.addChild(circle);
            }
        },

        addShadow:function() {
            this.shadow = utils.initCircle(0, 0, 15, this.model.get('side').color);
            var blurFilter = new easel.BlurFilter(40, 40, 2);
            this.shadow.filters = [blurFilter];
            var bounds = blurFilter.getBounds();
            this.shadow.cache(-50+bounds.x, -50+bounds.y, 100+bounds.width, 100+bounds.height);
            this.container.addChild(this.shadow);
        },

        animation: function (deltaTime) {
            var distance = deltaTime * this.model.get('speed') / 1000;
            if (move(this.container, this.model.get('toColony').getPoint(), distance)
                || this.model.get('population') <= 0) {
                if (this.model.get('population')) {
                    this.trigger('attackColony');
                }
            }

            _.each(this.circles, function (circle) {
                circle.x += [-0.45, -0.3, 0, 0, 0, 0.3, 0.45][_.random(0, 6)];
                circle.y += [-0.45, -0.3, 0, 0, 0, 0.3, 0.45][_.random(0, 6)];
            });
            this.text.text = this.model.get('population');
        }
    });

    function move(startPos, endPos, distancePerMove) {
        var totalDistance = utils.getDistance(startPos, endPos);
        startPos.x += (distancePerMove * (endPos.x - startPos.x)) / totalDistance;
        startPos.y += (distancePerMove * (endPos.y - startPos.y)) / totalDistance;

        var accuracy = distancePerMove * 2 + 10;
        return utils.getDistance(startPos, endPos) < accuracy;
    }
});