define([
    'underscore',
    'easel',
    'js/views/View',
    'js/utils',
    'js/game'
], function (_, easel, View, utils, game) {

    return View.extend({
        mousePosition: null,

        render: function () {
            View.prototype.render.call(this);
            this.container.x = this.model.get('x');
            this.container.y = this.model.get('y');
            this.color = this.model.get('side').color;
            this.line = null;

            game.field.onmousemove = function(e) {
                var doc = document.documentElement;
                var windowPosY = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
                this.mousePosition = {
                    x: e.x - this.container.x - game.field.offsetLeft,
                    y: e.y - this.container.y - game.field.offsetTop + windowPosY
                };
            }.bind(this);

            document.onmouseup = this.destroy.bind(this);
        },

        animation: function () {
            if (this.line) {
                this.container.removeChild(this.line);
            }
            if (this.mousePosition) {
                var distance = utils.getDistance({x: 0, y: 0}, this.mousePosition);
                var startPoint = {
                    x: (30 * this.mousePosition.x) / distance,
                    y: (30 * this.mousePosition.y) / distance
                };
                this.line = utils.drawLine(startPoint, this.mousePosition, 3, this.color);
                this.line.alpha = 0.3;
                this.container.addChild(this.line);
            }
        }
    });
});