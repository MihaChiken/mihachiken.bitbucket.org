define([
    'js/models/Model',
    'js/models/sides'
], function(Model, sides) {
    return Model.extend({
        defaults: {
            x: 0,
            y: 0,
            population: 10,
            decreaseTime: 400,
            speed: 110,
            fromColony: null,
            toColony: null,
            side: null,
            populationDivider: 1
        },

        initialize: function() {
            if(!this.get('fromColony') || !this.get('toColony')) {
                throw 'Both colonies an must be specified';
            }

            this.set({
                x: this.get('fromColony').get('x'),
                y: this.get('fromColony').get('y'),
                side: this.get('fromColony').get('side'),
                population: Math.floor(this.get('fromColony').get('population') / this.get('populationDivider'))
            });

            var sideOptions = this.get('side').options;
            if (sideOptions) {
                this.set(sideOptions);
            }
        },

        populationUpdate: function() {
            if (this.get('population') > 1) {
                this.set('population', this.get('population') - 1);
                return true;
            }
            return false;
        },

        getPoint: function() {
            return {
                x: this.get('x'),
                y: this.get('y')
            };
        }
    });
});