define([
    'underscore',
    'js/models/Model',
    'js/models/sides'
], function(_, Model, sides) {
    return Model.extend({
        defaults: {
            x: 0,
            y: 0,
            side: sides.default,
            population: 10,
            growthTime: 400,
            limit: 100
        },

        initialize: function() {
            var sideOptions = this.get('side').colonyOptions;
            if (sideOptions) {
                this.extendSideOptions();
            }

            this.on('change:side', this.extendSideOptions, this);
        },

        extendSideOptions: function() {
            if (this.previous('side')) {
                var defaultProps = {};
                _.each(this.previous('side').colonyOptions, function(value, attrName) {
                    defaultProps[attrName] = this.defaults[attrName];
                }, this);
                this.set(defaultProps);
            }
            this.set(this.get('side').colonyOptions);
        },

        populationGrowth: function() {
            var population = this.get('population');
            if (population < this.get('limit')) {
                this.set('population', population + 1);
            }
        },

        getPoint: function() {
            return {
                x: this.get('x'),
                y: this.get('y')
            };
        }
    });
});