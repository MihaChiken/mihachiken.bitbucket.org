define([], function() {
    return {
        purple: new Side('#aa33dd'),
        green:  new Side('#70c602'),
        pink:   new Side('#C60C63'),
        brown:  new Side('#8B8549'),

        'default': new Side('#777777', {
            growthTime: -1
        }),

        getPlayerSide: function() {
            return this.purple;
        }
    };

    function Side(color, colonyOptions, spawnOptions) {
        this.color = color;
        this.colonyOptions = colonyOptions || {};
        this.spawnOptions = spawnOptions || {};
    }
});